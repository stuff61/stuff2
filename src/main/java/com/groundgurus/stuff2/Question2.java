package com.groundgurus.stuff2;

import java.util.Scanner;

public class Question2 {

  public static void main(String[] args) {
    int[] inputs = getUserInputs();
    sort(inputs);
    inputs = removeDuplicates(inputs, inputs.length);

    for (int input : inputs) {
      System.out.println(input);
    }
  }

  private static int[] getUserInputs() {
    Scanner in = new Scanner(System.in);
    int[] arr = new int[1];
    String input;
    int count = 0;

    do {
      System.out.print("Enter inputs (type q to quit): ");
      input = in.next();

      try {
        int value = Integer.parseInt(input.trim());

        if (count >= arr.length) {
          int[] temp = new int[arr.length + 1];
          for (int i = 0; i < arr.length; i++) {
            temp[i] = arr[i];
          }
          arr = temp;
        }

        arr[count] = value;
        count++;
      } catch (NumberFormatException e) {
      }
    } while (!input.equals("q"));

    return arr;
  }

  private static void sort(int[] arr) {
    for (int i = 0; i < arr.length; i++) {
      int temp;
      for (int j = i + 1; j < arr.length; j++) {
        if (arr[i] < arr[j]) {
          temp = arr[i];
          arr[i] = arr[j];
          arr[j] = temp;
        }
      }
    }
  }

  public static int[] removeDuplicates(int a[], int n) {
    int j = 0;

    for (int i = 0; i < n - 1; i++) {
      if (a[i] != a[i + 1]) {
        a[j++] = a[i];
      }
    }

    a[j++] = a[n - 1];

    int[] temp = new int[j];
    for (int i = 0; i < j; i++) {
     temp[i] = a[i];
    }

    return temp;
  }
}
