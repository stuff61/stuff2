package com.groundgurus.stuff2;

import java.util.Arrays;
import java.util.Scanner;

public class BullsAndCowsTaskOne {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    String secretCode;

    while (true) {
      secretCode = enterSecretCode(scanner);

      if (!isCodeValid(secretCode)) {
        System.out.println("Code should be 4 digits and the number do not repeat");
      } else {
        break;
      }
    }

    System.out.println("---");

    while (true) {
      String guess = enterGuess(scanner);

      if (!isCodeValid(guess)) {
        System.out.println("Code should be 4 digits and the number do not repeat");
        continue;
      }

      int bulls = countBulls(secretCode, guess);
      int cows = countCows(secretCode, guess);
      printBullsAndCowsResult(bulls, cows);

      if (bulls == secretCode.length()) {
        System.out.println("You win! :)");
        break;
      }
    }
  }

  private static void printBullsAndCowsResult(long bulls, long cows) {
    StringBuilder sb = new StringBuilder();

    sb.append(bulls);
    if (bulls > 1) {
      sb.append(" bull");
    } else {
      sb.append(" bulls");
    }

    sb.append(" and ");
    sb.append(cows);
    if (cows > 1) {
      sb.append(" cow");
    } else {
      sb.append(" cows");
    }

    System.out.println(sb);
  }

  private static boolean isCodeValid(String code) {
    String[] codes = code.split("");
    long count = Arrays.stream(codes)
        .map(s -> s.charAt(0))
        .filter(Character::isDigit)
        .distinct()
        .count();
    return count == 4;
  }

  private static String enterSecretCode(Scanner scanner) {
    System.out.print("Please enter your secret code: ");
    return scanner.next();
  }

  private static String enterGuess(Scanner scanner) {
    System.out.print("Your guess: ");
    return scanner.next();
  }

  private static int countBulls(String secretCode, String userInput) {
    String[] sCode = secretCode.split("");
    String[] input = userInput.split("");
    int count = 0;

    for (int i = 0; i < input.length; i++) {
      if (input[i].equals(sCode[i])) {
        count++;
      }
    }

    return count;
  }

  private static int countCows(String secretCode, String userInput) {
    int count = 0;
    String[] input = userInput.split("");

    for (int i = 0; i < input.length; i++) {
      if (secretCode.contains(input[i]) && secretCode.indexOf(input[i]) != i) {
        count++;
      }
    }

    return count;
  }
}
