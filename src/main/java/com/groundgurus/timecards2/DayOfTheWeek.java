package com.groundgurus.timecards2;

import java.util.Arrays;

public enum DayOfTheWeek {
  MON(1), TUE(2), WED(3), THU(4), FRI(5), SAT(6), SUN(7);
  private final int day;

  DayOfTheWeek(int day) {
    this.day = day;
  }

  public int getDay() {
    return day;
  }

  public static DayOfTheWeek find(int day) {
    return Arrays.stream(values())
        .filter(dayOfTheWeek -> dayOfTheWeek.getDay() == day)
        .findFirst()
        .orElse(null);
  }
}
