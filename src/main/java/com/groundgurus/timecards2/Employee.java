package com.groundgurus.timecards2;

import java.math.BigDecimal;

public class Employee {

  private String fullName;
  private BigDecimal salary;

  public Employee() {
  }

  public Employee(String fullName) {
    this.fullName = fullName;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public BigDecimal getSalary() {
    return salary;
  }

  public void setSalary(BigDecimal salary) {
    this.salary = salary;
  }
}
