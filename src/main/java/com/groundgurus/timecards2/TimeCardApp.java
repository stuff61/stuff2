package com.groundgurus.timecards2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TimeCardApp {

  public static void main(String[] args) throws IOException {
    Path path = Paths.get(
        System.getProperty("user.home") + File.separator + "Desktop" + File.separator
            + "payroll.txt");

    Payroll payroll = new Payroll(path);
    payroll.calculatePayroll();

    payroll.getEmployees()
        .stream()
        .map(TimeCardApp::getFormattedOutput)
        .forEach(System.out::println);
  }

  private static String getFormattedOutput(Employee employee) {
    return String.format("%s: $%.2f", employee.getFullName(), employee.getSalary());
  }
}
