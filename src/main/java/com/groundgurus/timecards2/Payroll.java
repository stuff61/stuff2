package com.groundgurus.timecards2;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Payroll {

  private final List<Employee> employees = new ArrayList<>();
  private List<String> rawData = new ArrayList<>();

  public Payroll(Path path) throws IOException {
    readPayrollData(path);
  }

  private void readPayrollData(Path path) throws IOException {
    rawData = Files.readAllLines(path);
  }

  public void calculatePayroll() {
    for (int i = 0; i < rawData.size(); i++) {
      String rawData = this.rawData.get(i);
      Employee employee = new Employee("Employee " + (i + 1));
      BigDecimal salary = getCalculatedEmployeeSalary(rawData);
      employee.setSalary(salary);
      employees.add(employee);
    }
  }

  private BigDecimal getCalculatedEmployeeSalary(String data) {
    BigDecimal salary = BigDecimal.ZERO;
    String[] dataPerDay = data.split(",");
    int hoursWorkedInAWeek = 0;

    for (int i = 0; i < dataPerDay.length; i++) {
      String dataOfTheDay = dataPerDay[i];
      int hoursWorked = Integer.parseInt(dataOfTheDay);
      hoursWorkedInAWeek += hoursWorked;
      DayOfTheWeek dayOfTheWeek = DayOfTheWeek.find(i + 1);

      switch (dayOfTheWeek) {
        case SAT:
          salary = salary.add(getSaturdayPay(hoursWorked));
          break;
        case SUN:
          salary = salary.add(getSundayPay(hoursWorked));
          break;
        default:
          salary = salary.add(getPayOfTheDay(hoursWorked, hoursWorkedInAWeek));
      }
    }

    return salary;
  }

  private BigDecimal getPayOfTheDay(int hoursWorked, int hoursWorkedInAWeek) {
    BigDecimal salary = BigDecimal.ZERO;

    if (hoursWorked <= 8) {
      salary = salary.add(getRegularHoursPay(hoursWorked));
    } else {
      salary = salary.add(getRegularHoursWithOvertimePay(hoursWorked));
    }

    if (hoursWorkedInAWeek > 40) {
      salary = salary.add(getExtraPay(hoursWorked));
    }

    return salary;
  }

  private BigDecimal getSaturdayPay(int hoursWorked) {
    return getRegularHoursPay(hoursWorked)
        .multiply(BigDecimal.valueOf(1.25));
  }

  private BigDecimal getSundayPay(int hoursWorked) {
    return getRegularHoursPay(hoursWorked)
        .multiply(BigDecimal.valueOf(1.50));
  }

  private BigDecimal getExtraPay(int hoursWorked) {
    return new BigDecimal(hoursWorked * 2.5);
  }

  private BigDecimal getRegularHoursWithOvertimePay(int hoursWorked) {
    return getRegularHoursPay(8).add(getOvertimePay(hoursWorked - 8));
  }

  private BigDecimal getRegularHoursPay(int hoursWorked) {
    return BigDecimal.valueOf(hoursWorked * 10.0);
  }

  private BigDecimal getOvertimePay(int hoursWorked) {
    return BigDecimal.valueOf(hoursWorked * 11.5);
  }

  public List<Employee> getEmployees() {
    return employees;
  }
}
