//Fermin Patdu Jr.
package com.groundgurus.timecards;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadLines {

  public static void readLines(File file) throws IOException {
    FileReader fr = new FileReader(file);
    BufferedReader br = new BufferedReader(fr);
    String line;
    float salary;
    float otSat;
    float otSun;

    int ctr = 1; // numbering for output
    int i;
    try {

      while ((line = br.readLine()) != null) {
        System.out.print("Output " + ctr++ + ": $");
        String[] values = line.split(",");

        int weekDayTotal = 0; // weekdays total
        salary = 0.00F;

        for (i = 1; i < 6; i++) // mon- fri - 5 times looping for weekdays
        {
          // extra above 8 hrs
          if (Integer.parseInt(values[i]) >= 8) {
            int ot = (Integer.parseInt(values[i]) - 8);
            salary += Float.parseFloat(values[i]) * 10 + (ot * 1.50);
          } else {
            salary += Float.parseFloat(values[i]) * 10; // 10 per hour
          }
          weekDayTotal += (Float.parseFloat(values[i]));
        }
        if (weekDayTotal > 40) {
          salary += (weekDayTotal - 40) * 2.50;
        }

        {
          if (Integer.parseInt(values[0]) >= 8 && (Integer.parseInt(values[6]) >= 8)) {
            otSat = (Integer.parseInt(values[0]) - 8);
            salary += (Float.parseFloat(values[0]) * 10 + (otSat * 1.50)) * 2.25;
            if (Integer.parseInt(values[0]) > 8) {
              otSun = (Integer.parseInt(values[6]) - 8);
              salary += (Float.parseFloat(values[6]) * 1.5 + (otSun * 1.50)) * 10;

            }

          } else if (Integer.parseInt(values[6]) >= 8) {
            otSun = (Integer.parseInt(values[6]) - 8);
            salary += (Float.parseFloat(values[6]) * 10 + (otSun * 1.50)) * 2.25;

          } else {
            salary +=
                (1.5) * Float.parseFloat(values[0]) * 10; //sal for sunday along with 50% bonus
            salary += (2.25) * Float.parseFloat(values[6])
                * 10; // sal for saturday along wit 125 % bonus
          }

          System.out.println(salary);
        }
      }
    } catch (FileNotFoundException e) {
      br.close();
      fr.close();
    }
  }

  public static void main(String[] args) throws IOException {
    File file = new File(
        System.getProperty("user.home") + File.separator + "Desktop" + File.separator + "f.txt");
    readLines(file);
  }
}
